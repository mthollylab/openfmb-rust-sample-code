# Cross compile for ARMv7 from AMD64

## NOTE

The `config` file in this folder tells `cargo` to use the ARM-specific version of `gcc` for linking binaries compiled for the `armv7-unknown-linux-gnueabihf` Rust target. This could be extended for additional platforms if necessary.

## Steps

First, install the Rust target platform for ARMv7:

`rustup target add armv7-unknown-linux-gnueabihf`

Next, if you are using a Debian/Ubuntu based OS, install the ARMv7 compiler and linker using the following command:

`sudo apt install gcc-arm-linux-gnueabihf`

You should be able to run `which arm-linux-gnueabihf-gcc` to see that the tools are installed.

You can then cross compile to ARMv7 by running the following from the project root folder:

`cargo build --target=armv7-unknown-linux-gnueabihf`
