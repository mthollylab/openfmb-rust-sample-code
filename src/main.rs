use std::str::FromStr;

extern crate clap;
use clap::{Arg, Command};

extern crate nats;
extern crate nkeys;

use uuid::Uuid;

extern crate ctrlc;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::thread;
use std::time;

mod breaker_module;
mod meter_module;
mod resource_module;
mod solar_module;
mod switch_module;

// extern crate rust_openfmb_ops_protobuf;
// use rust_openfmb_ops_protobuf::openfmb::metermodule::MeterReadingProfile;
mod openfmb;

fn load_jwt() -> std::io::Result<String> {
    let jwt_path = std::env::var("NATS_JWT_PATH").unwrap();

    let path = std::path::Path::new(&jwt_path);

    std::fs::read_to_string(path)
}

fn load_seed(n: &[u8]) -> Vec<u8> {
    let seed = "";
    let kp = nkeys::KeyPair::from_seed(seed).unwrap();

    return kp.sign(n).unwrap();
}

fn main() {
    let matches = Command::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .arg(
            Arg::new("SERVER")
                .short('s')
                .long("server")
                .value_name("URL")
                .help("NATS broker server URL")
                .takes_value(true)
                .required(false)
                .default_value("localhost:4222")
        )
        .arg(
            Arg::new("NATS_JWT_PATH")
                .short('j')
                .long("jwt")
                .help("Path to JWT for authentcated connection to NATS")
                .takes_value(true)
                .required(false)
        )
        .arg(
            Arg::new("NATS_CREDS_PATH")
                .short('c')
                .long("creds")
                .help("Path to credentials file for authentcated connection to NATS")
                .takes_value(true)
                .required(false)
        )
        .arg(
            Arg::new("OPENFMB_PROFILE")
                .short('p')
                .long("profile")
                .help("OpenFMB Profile (meter_reading, breaker_reading, switch_reading, resource_status)")
                .takes_value(true)
                .required(false)
                .default_value("meter_reading")
        )
        .arg(
            Arg::new("MRID")
                .short('m')
                .long("mrid")
                .help("mRID of meter to subscribe to for messages")
                .takes_value(true)
                .required(false)
                .default_value(">")
        )
        .arg(
            Arg::new("PRINT_DATA")
                .short('d')
                .long("printdata")
                .help("Prints OpenFMB message data for each received message")
                .required(false)
        )
        .get_matches();

    // Gets a value for config if supplied by user, or defaults to "default.conf"
    let nats_server = matches.value_of("SERVER").unwrap();
    println!("Value for SERVER: {}", nats_server);

    let mrid = matches.value_of("MRID").unwrap();

    // Make sure that the MRID parameter is a valid UUIDv4 or a NATS wildcard character
    if mrid == ">" || mrid == "*" {
        // NATS wildcard character...Ok
    } else {
        match Uuid::from_str(&mrid) {
            Ok(ok) => {
                if ok.get_version_num() != 4 {
                    println!("MRID version is invalid.");
                    std::process::exit(1);
                }
            }
            Err(e) => {
                println!("MRID pararmeter is not valid!\nParseError: {}", e);
                std::process::exit(1);
            }
        }
    }

    // Make sure we have a valid profile so we can set the NATS topic correctly.
    let topic: std::string::String;

    let profile = matches.value_of("OPENFMB_PROFILE").unwrap();
    if profile == "meter_reading" {
        topic = "openfmb.metermodule.MeterReadingProfile.".to_owned() + mrid;
    } else if profile == "breaker_reading" {
        topic = "openfmb.breakermodule.BreakerReadingProfile.".to_owned() + mrid;
    } else if profile == "breaker_status" {
        topic = "openfmb.breakermodule.BreakerStatusProfile.".to_owned() + mrid;
    } else if profile == "switch_reading" {
        topic = "openfmb.switchmodule.SwitchReadingProfile.".to_owned() + mrid;
    } else if profile == "solar_reading" {
        topic = "openfmb.solarmodule.SolarReadingProfile.".to_owned() + mrid;
    } else if profile == "resource_status" {
        topic = "openfmb.resourcemodule.ResourceStatusProfile.".to_owned() + mrid;
    } else {
        println!(
            "{}",
            "Invalid profile. Must be:\n".to_owned()
                + "  \"meter_reading\"\n"
                + "  \"breaker_reading\"\n"
                + "  \"breaker_status\"\n"
                + "  \"switch_reading\"\n"
                + "  \"solar_reading\"\n"
                + "  \"resource_status\""
        );
        std::process::exit(1);
    }
    println!("Subscription topic: {}", &topic);

    let options: nats::Options;
    if matches.is_present("NATS_JWT_PATH") {
        let jwt = matches.value_of("NATS_JWT_PATH").unwrap();
        std::env::set_var("NATS_JWT_PATH", jwt);
        options = nats::Options::with_jwt(load_jwt, move |nonce| load_seed(nonce));
    } else if matches.is_present("NATS_CREDS_PATH") {
        let creds = matches.value_of("NATS_CREDS_PATH").unwrap();

        options = nats::Options::with_credentials(creds);
    } else {
        options = nats::Options::new();
    }

    let nc: nats::Connection;
    let nc_r: Result<nats::Connection, std::io::Error>;
    nc_r = options
        .with_name("OpenFMB Rust Sample Code")
        .connect(nats_server);
    match nc_r {
        Ok(nc_ok) => {
            println!("Successfully connected to NATS server...");
            nc = nc_ok;
        }
        Err(e) => {
            println!("Connection to NATS server failed: {}", e);
            std::process::exit(1);
        }
    };

    // let nsh: nats::subscription::Handler;
    let nsr = nc.subscribe(topic.as_str());
    let ns: nats::Subscription;
    match nsr {
        Ok(nsr_ok) => {
            ns = nsr_ok;
        }
        Err(e) => {
            println!("Could not create subscription: {}", e);
            std::process::exit(2);
        }
    }

    let print_data = matches.is_present("PRINT_DATA");

    if profile == "meter_reading" {
        let _nsh = ns.with_handler(move |msg| {
            return meter_module::meter_reading_profile::subscribe_handler(msg, print_data);
        });
    } else if profile == "breaker_reading" {
        let _nsh = ns.with_handler(move |msg| {
            return breaker_module::breaker_reading_profile::subscribe_handler(msg, print_data);
        });
    } else if profile == "breaker_status" {
        let _nsh = ns.with_handler(move |msg| {
            return breaker_module::breaker_status_profile::subscribe_handler(msg, print_data);
        });
    } else if profile == "switch_reading" {
        let _nsh = ns.with_handler(move |msg| {
            return switch_module::switch_reading_profile::subscribe_handler(msg, print_data);
        });
    } else if profile == "solar_reading" {
        let _nsh = ns.with_handler(move |msg| {
            return solar_module::solar_reading_profile::subscribe_handler(msg, print_data);
        });
    } else if profile == "resource_status" {
        let _nsh = ns.with_handler(move |msg| {
            return resource_module::resource_status_profile::subscribe_handler(msg, print_data);
        });
    } else {
        std::process::exit(2);
    }

    let running = Arc::new(AtomicBool::new(true));
    let r = running.clone();
    ctrlc::set_handler(move || {
        r.store(false, Ordering::SeqCst);
    })
    .expect("Error setting Ctrl-C handler");
    println!("Waiting for Ctrl-C...");
    while running.load(Ordering::SeqCst) {
        thread::sleep(time::Duration::from_millis(10));
    }
    println!("Got it! Exiting...");

    nc.close();
    println!("NATS connection close() successful!");
}
