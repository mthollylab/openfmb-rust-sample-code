use std::io;

extern crate prost;
use prost::alloc::vec::Vec;
use prost::bytes::Bytes;
use prost::Message;

use crate::openfmb::commonmodule::*;
use crate::openfmb::switchmodule::{SwitchReading, SwitchReadingProfile};

pub fn subscribe_handler(msg: nats::Message, print_data: bool) -> io::Result<()> {
    println!("\nSubject: {}", msg.subject);
    // println!("Subscription ID: {}", msg.subscription_id);
    println!("Payload Size: {}", msg.data.len());
    let r = SwitchReadingProfile::decode(Bytes::from(msg.data.to_owned()));
    match r {
        Ok(openfmb_message) => {
            if print_data {
                println!("SwitchReadingProfile data: {:#?}", openfmb_message);
            }

            let rmi: Option<ReadingMessageInfo> = openfmb_message.reading_message_info;

            if rmi.is_some() {
                let rmi_val: ReadingMessageInfo = rmi.unwrap();
                let mi: MessageInfo = rmi_val.message_info.unwrap();
                let io: IdentifiedObject = mi.identified_object.unwrap();
                let mrid: String = io.m_rid.unwrap();
                println!("Message mRID: {}", mrid);
                let ts: Timestamp = mi.message_time_stamp.unwrap();

                // Print the timestamp info
                println!("Message Timestamp: {}.{}", ts.seconds, ts.nanoseconds);
            }

            let switch_name: Option<String> = openfmb_message
                .protected_switch
                .unwrap_or_default()
                .conducting_equipment
                .unwrap_or_default()
                .named_object
                .unwrap_or_default()
                .name;

            if switch_name.is_some() {
                let name: String = switch_name.unwrap();
                println!("Switch name: {}", name);
            }

            let mut index = 0;

            let sra: Vec<SwitchReading> = openfmb_message.switch_reading;
            for x in sra {
                let sr: SwitchReading = x;
                println!("SwtichReading index: {}", index);
                index += 1;

                let w_net_cval = sr
                    .to_owned()
                    .reading_mmxu
                    .unwrap_or_default()
                    .w
                    .unwrap_or_default()
                    .net
                    .unwrap_or_default()
                    .c_val;
                if w_net_cval.is_some() {
                    // println!("We have a ReadingMMXU.W.Net.cVal.mag value!!!");
                    println!("  ReadingMMXU.W.Net.cVal.mag: {}", w_net_cval.unwrap().mag);
                }

                let var_net_cval = sr
                    .to_owned()
                    .reading_mmxu
                    .unwrap_or_default()
                    .v_ar
                    .unwrap_or_default()
                    .net
                    .unwrap_or_default()
                    .c_val;
                if var_net_cval.is_some() {
                    // println!("We have a ReadingMMXU.VAr.Net.cVal.mag value!!!");
                    println!(
                        "  ReadingMMXU.VAr.Net.cVal.mag.f: {}",
                        var_net_cval.unwrap().mag
                    );
                }
            }
        }
        Err(e) => {
            // Protobuf did not decode properly!
            println!("{}", e);
        }
    }
    Ok(())
}
