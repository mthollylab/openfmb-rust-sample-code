use std::io;

extern crate prost;
use prost::bytes::Bytes;
use prost::Message;

use crate::openfmb::commonmodule::*;
use crate::openfmb::solarmodule::{SolarInverter, SolarReading, SolarReadingProfile};

pub fn subscribe_handler(msg: nats::Message, print_data: bool) -> io::Result<()> {
    println!("\nSubject: {}", msg.subject);
    // println!("Subscription ID: {}", msg.subscription_id);
    println!("Payload Size: {}", msg.data.len());
    let r = SolarReadingProfile::decode(Bytes::from(msg.data.to_owned()));
    match r {
        Ok(openfmb_message) => {
            if print_data {
                println!("SolarReadingProfile: {:#?}", openfmb_message);
            }
            let rmi_opt: Option<ReadingMessageInfo> = openfmb_message.reading_message_info;
            if rmi_opt.is_some() {
                let rmi: ReadingMessageInfo = rmi_opt.unwrap();
                let mi: MessageInfo = rmi.message_info.unwrap();
                let io: IdentifiedObject = mi.identified_object.unwrap();
                let mrid: String = io.m_rid.unwrap();
                println!("Message mRID: {}", mrid);

                let ts: Timestamp = mi.message_time_stamp.unwrap();
                // Print the timestamp info
                println!("Message Timestamp: {}.{}", ts.seconds, ts.nanoseconds);
            }

            let si_opt: Option<SolarInverter> = openfmb_message.solar_inverter;
            match si_opt {
                Some(solar_inverter) => {
                    let ce_opt: Option<ConductingEquipment> = solar_inverter.conducting_equipment;

                    match ce_opt {
                        Some(ce) => {
                            let no_opt: Option<NamedObject> = ce.named_object;

                            match no_opt {
                                Some(no) => {
                                    let name: Option<String> = no.name;

                                    match name {
                                        Some(name) => {
                                            println!("Solar name: {}", name);
                                        }
                                        None => {}
                                    }
                                }
                                None => {}
                            }
                        }
                        None => {}
                    }
                }
                None => {}
            }

            let mut index = 0;

            let sr_vec: Option<SolarReading> = openfmb_message.solar_reading;
            for sr in sr_vec {
                println!("SolarReading index: {}", index);
                index += 1;

                let reading_mmxu_opt: Option<ReadingMmxu> = sr.reading_mmxu;
                match reading_mmxu_opt {
                    Some(reading_mmxu) => {
                        let w_net_opt: Option<Vector> = reading_mmxu
                            .w
                            .unwrap_or_default()
                            .net
                            .unwrap_or_default()
                            .c_val;
                        match w_net_opt {
                            Some(w_net_c_val) => {
                                println!("  ReadingMMXU.W.Net.cVal: {}", w_net_c_val.mag);
                            }
                            None => {}
                        }

                        let var_net_opt: Option<Vector> = reading_mmxu
                            .v_ar
                            .unwrap_or_default()
                            .net
                            .unwrap_or_default()
                            .c_val;
                        match var_net_opt {
                            Some(var_net_c_val) => {
                                println!("  ReadingMMXU.VAr.Net.cVal.mag: {}", var_net_c_val.mag);
                            }
                            None => {}
                        }
                    }
                    None => {}
                }
            }
        }
        Err(e) => {
            // Protobuf did not decode properly!
            println!("{}", e);
        }
    }
    Ok(())
}
