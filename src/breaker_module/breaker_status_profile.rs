use std::io;

extern crate prost;
use prost::bytes::Bytes;
use prost::Message;

// extern crate rust_openfmb_ops_protobuf;
// use rust_openfmb_ops_protobuf::openfmb::breakermodule::{BreakerStatusProfile, Breaker, BreakerStatus};
// use rust_openfmb_ops_protobuf::openfmb::commonmodule::*;
use crate::openfmb::breakermodule::{Breaker, BreakerStatus, BreakerStatusProfile};
use crate::openfmb::commonmodule::*;

pub fn subscribe_handler(msg: nats::Message, print_data: bool) -> io::Result<()> {
    println!("\nSubject: {}", msg.subject);
    // println!("Subscription ID: {}", msg.subscription_id);
    println!("Payload Size: {}", msg.data.len());
    let r = BreakerStatusProfile::decode(Bytes::from(msg.data.to_owned()));
    match r {
        Ok(openfmb_message) => {
            if print_data {
                println!("BreakerStatusProfile: {:#?}", openfmb_message);
            }
            let smi_opt: Option<StatusMessageInfo> = openfmb_message.status_message_info;
            if smi_opt.is_some() {
                let smi: StatusMessageInfo = smi_opt.unwrap();
                let mi: MessageInfo = smi.message_info.unwrap();
                let io: IdentifiedObject = mi.identified_object.unwrap();
                let mrid: String = io.m_rid.unwrap();
                println!("Message mRID: {}", mrid);

                let ts: Timestamp = mi.message_time_stamp.unwrap();
                // Print the timestamp info
                println!("Message Timestamp: {}.{}", ts.seconds, ts.nanoseconds);
            }

            let breaker_opt: Option<Breaker> = openfmb_message.breaker;
            match breaker_opt {
                Some(breaker) => {
                    let ce_opt: Option<ConductingEquipment> = breaker.conducting_equipment;

                    match ce_opt {
                        Some(ce) => {
                            let no_opt: Option<NamedObject> = ce.named_object;

                            match no_opt {
                                Some(no) => {
                                    let name: Option<String> = no.name;

                                    match name {
                                        Some(name) => {
                                            println!("Breaker name: {}", name);
                                        }
                                        None => {}
                                    }
                                }
                                None => {}
                            }
                        }
                        None => {}
                    }
                }
                None => {}
            }

            let bs_opt: Option<BreakerStatus> = openfmb_message.breaker_status;
            match bs_opt {
                Some(bs) => {
                    let xcbr_opt: Option<StatusAndEventXcbr> = bs.status_and_event_xcbr;
                    match xcbr_opt {
                        Some(xcbr) => {
                            let pos_opt = xcbr.pos;
                            match pos_opt {
                                Some(pos) => {
                                    let db_pos_kind_index = pos.phs3.unwrap_or_default().st_val;

                                    let db_pos_kind: DbPosKind;
                                    match db_pos_kind_index {
                                        0 => {
                                            db_pos_kind = DbPosKind::Undefined;
                                        }
                                        1 => {
                                            db_pos_kind = DbPosKind::Transient;
                                        }
                                        2 => {
                                            db_pos_kind = DbPosKind::Closed;
                                        }
                                        3 => {
                                            db_pos_kind = DbPosKind::Open;
                                        }
                                        4 => {
                                            db_pos_kind = DbPosKind::Invalid;
                                        }
                                        _ => {
                                            db_pos_kind = DbPosKind::Undefined;
                                        }
                                    }
                                    println!(
                                        "  StatusAndEventXCBR.Pos: {} ({:?})",
                                        db_pos_kind_index, db_pos_kind
                                    );
                                }
                                None => {}
                            }
                        }
                        None => {}
                    }
                }
                None => {}
            }
        }
        Err(e) => {
            // Protobuf did not decode properly!
            println!("{}", e);
        }
    }
    Ok(())
}
