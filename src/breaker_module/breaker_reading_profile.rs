use std::io;

extern crate prost;
use prost::bytes::Bytes;
use prost::Message;

// extern crate rust_openfmb_ops_protobuf;
// use rust_openfmb_ops_protobuf::openfmb::breakermodule::{BreakerReadingProfile, Breaker, BreakerReading};
// use rust_openfmb_ops_protobuf::openfmb::commonmodule::*;
use crate::openfmb::breakermodule::{Breaker, BreakerReading, BreakerReadingProfile};
use crate::openfmb::commonmodule::*;

pub fn subscribe_handler(msg: nats::Message, print_data: bool) -> io::Result<()> {
    println!("\nSubject: {}", msg.subject);
    // println!("Subscription ID: {}", msg.subscription_id);
    println!("Payload Size: {}", msg.data.len());
    let r = BreakerReadingProfile::decode(Bytes::from(msg.data.to_owned()));
    match r {
        Ok(openfmb_message) => {
            if print_data {
                println!("BreakerReadingProfile: {:#?}", openfmb_message);
            }
            let rmi_opt: Option<ReadingMessageInfo> = openfmb_message.reading_message_info;
            if rmi_opt.is_some() {
                let rmi: ReadingMessageInfo = rmi_opt.unwrap();
                let mi: MessageInfo = rmi.message_info.unwrap();
                let io: IdentifiedObject = mi.identified_object.unwrap();
                let mrid: String = io.m_rid.unwrap();
                println!("Message mRID: {}", mrid);

                let ts: Timestamp = mi.message_time_stamp.unwrap();
                // Print the timestamp info
                println!("Message Timestamp: {}.{}", ts.seconds, ts.nanoseconds);
            }

            let breaker_opt: Option<Breaker> = openfmb_message.breaker;
            match breaker_opt {
                Some(breaker) => {
                    let ce_opt: Option<ConductingEquipment> = breaker.conducting_equipment;

                    match ce_opt {
                        Some(ce) => {
                            let no_opt: Option<NamedObject> = ce.named_object;

                            match no_opt {
                                Some(no) => {
                                    let name: Option<String> = no.name;

                                    match name {
                                        Some(name) => {
                                            println!("Breaker name: {}", name);
                                        }
                                        None => {}
                                    }
                                }
                                None => {}
                            }
                        }
                        None => {}
                    }
                }
                None => {}
            }

            let mut index = 0;

            let br_vec: Vec<BreakerReading> = openfmb_message.breaker_reading;
            for br in br_vec {
                println!("BreakerReading index: {}", index);
                index += 1;

                let reading_mmxu_opt: Option<ReadingMmxu> = br.reading_mmxu;
                match reading_mmxu_opt {
                    Some(reading_mmxu) => {
                        let w_net_opt: Option<Vector> = reading_mmxu
                            .w
                            .unwrap_or_default()
                            .net
                            .unwrap_or_default()
                            .c_val;
                        match w_net_opt {
                            Some(w_net_c_val) => {
                                println!("  ReadingMMXU.W.Net.cVal.mag: {}", w_net_c_val.mag);
                            }
                            None => {}
                        }

                        let var_net_opt: Option<Vector> = reading_mmxu
                            .v_ar
                            .unwrap_or_default()
                            .net
                            .unwrap_or_default()
                            .c_val;
                        match var_net_opt {
                            Some(var_net_c_val) => {
                                println!("  ReadingMMXU.VAr.Net.cVal.mag: {}", var_net_c_val.mag);
                            }
                            None => {}
                        }
                    }
                    None => {}
                }
            }
        }
        Err(e) => {
            // Protobuf did not decode properly!
            println!("{}", e);
        }
    }
    Ok(())
}
