use std::io;

extern crate prost;
use prost::bytes::Bytes;
use prost::Message;

extern crate serde;

use crate::openfmb::commonmodule::*;
use crate::openfmb::resourcemodule::ResourceStatusProfile;

pub fn subscribe_handler(msg: nats::Message, print_data: bool) -> io::Result<()> {
    println!("\nSubject: {}", msg.subject);
    // println!("Subscription ID: {}", msg.subscription_id);
    println!("Payload Size: {}", msg.data.len());

    let r = ResourceStatusProfile::decode(Bytes::from(msg.data.to_owned()));
    match r {
        Ok(openfmb_message) => {
            if print_data {
                println!("ResourceStatusProfile data: {:#?}", openfmb_message);
            }

            let rmi: Option<StatusMessageInfo> = openfmb_message.status_message_info;

            if rmi.is_some() {
                let rmi_val: StatusMessageInfo = rmi.unwrap();
                let mi: MessageInfo = rmi_val.message_info.unwrap();
                let io: IdentifiedObject = mi.identified_object.unwrap();
                let mrid: String = io.m_rid.unwrap();
                println!("Message mRID: {}", mrid);
                let ts: Timestamp = mi.message_time_stamp.unwrap();

                // Print the timestamp info
                println!("Message Timestamp: {}.{}", ts.seconds, ts.nanoseconds);
            }
        }
        Err(e) => {
            // Protobuf did not decode properly!
            println!("{}", e);
        }
    }
    Ok(())
}
