use std::io;

extern crate prost;
use prost::bytes::Bytes;
pub use prost::Message;

// extern crate rust_openfmb_ops_protobuf;
// use rust_openfmb_ops_protobuf::openfmb::metermodule::MeterReadingProfile;

pub use crate::openfmb::metermodule::MeterReadingProfile;

pub fn subscribe_handler(msg: nats::Message, print_data: bool) -> io::Result<()> {
    println!("\nSubject: {}", msg.subject);
    // println!("Subscription ID: {}", msg.subscription_id);
    println!("Payload Size: {}", msg.data.len());

    let r = MeterReadingProfile::decode(Bytes::from(msg.data.to_owned()));
    match r {
        Ok(openfmb_message) => {
            if print_data {
                println!("MeterReadingProfile: {:#?}", openfmb_message);
            }

            let rmi_opt = openfmb_message.reading_message_info;
            let mr_opt = openfmb_message.meter_reading;

            if rmi_opt.is_some() {
                let rmi = rmi_opt.unwrap();
                let mi = rmi.message_info.unwrap();
                let io = mi.identified_object.unwrap();
                let mrid = io.m_rid.unwrap();
                println!("Message mRID: {}", mrid);

                let ts = mi.message_time_stamp.as_ref().unwrap();

                // Print the timestamp info
                println!("Message Timestamp: {}.{}", ts.seconds, ts.nanoseconds);
            }

            if mr_opt.is_some() {
                let ppv_ab_cval = mr_opt
                    .clone()
                    .unwrap()
                    .reading_mmxu
                    .unwrap_or_default()
                    .ppv
                    .unwrap_or_default()
                    .phs_ab
                    .unwrap_or_default()
                    .c_val;

                if ppv_ab_cval.is_some() {
                    println!(
                        "  ReadingMMXU.PPV.PhsAB.cVal.mag: {}",
                        ppv_ab_cval.unwrap().mag
                    );
                }

                let w_net_cval = mr_opt
                    .unwrap()
                    .reading_mmxu
                    .unwrap_or_default()
                    .w
                    .unwrap_or_default()
                    .net
                    .unwrap_or_default()
                    .c_val;

                if w_net_cval.is_some() {
                    println!("  ReadingMMXU.W.Net.cVal.mag: {}", w_net_cval.unwrap().mag);
                }
            }
        }
        Err(e) => {
            // Protobuf did not decode properly!
            println!("{}", e);
        }
    }
    Ok(())
}
